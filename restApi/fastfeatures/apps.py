from django.apps import AppConfig


class FastfeaturesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fastfeatures'
